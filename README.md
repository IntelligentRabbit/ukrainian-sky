# Ptashechka: Ukrainian Sky :bird: :blue_heart: :yellow_heart:
Shoot-Em-Up game App for Android. Close the sky over Ukraine! Control a brave little bird. Destroy the evil russian missiles. Be as effective as possible. Stand with Ukraine!
Watch gameplay  [on YouTube.](https://youtu.be/jYxLFHLSiTE)


# Prerequisites
- [Python 3.6+](https://www.python.org/)
- [Kivy>=2.0.0](https://kivy.org/#home)
