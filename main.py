from random import randint

from kivy.app import App

from kivy.core.image import Image
from kivy.uix.widget import Widget
from kivy.core.text import LabelBase
from kivy.clock import Clock
from kivy.uix.image import Image as ImageWidget
from kivy.uix.popup import Popup
from kivy.animation import Animation
from kivy.utils import get_color_from_hex
from kivy.core.audio import SoundLoader
from kivy.properties import (
    AliasProperty,
    NumericProperty,
    ObjectProperty,
    BooleanProperty,
    StringProperty,
)


class BaseWidget(Widget):
    def load_tileable(self, name):
        t = Image(f"images/{name}.png").texture
        t.wrap = "repeat"
        setattr(self, f"tx_{name}", t)


class Background(BaseWidget):
    tx_floor = ObjectProperty(None)
    tx_grass = ObjectProperty(None)
    tx_cloud = ObjectProperty(None)
    floor_speed = NumericProperty(0)
    grass_speed = NumericProperty(0)

    def __init__(self, **kwargs):
        super(Background, self).__init__(**kwargs)
        for name in ("floor", "grass", "cloud"):
            self.load_tileable(name)

    def set_background_size(self, tx):
        tx.uvsize = (self.width / tx.width, -1)

    def on_size(self, *args):
        for tx in (self.tx_floor, self.tx_grass, self.tx_cloud):
            self.set_background_size(tx)

    def set_background_uv(self, name, val):
        t = getattr(self, name)
        t.uvpos = ((t.uvpos[0] + val) % self.width, t.uvpos[1])
        self.property(name).dispatch(self)

    def update(self, nap):
        self.set_background_uv("tx_cloud", 0.1 * nap)
        self.set_background_uv("tx_grass", self.grass_speed * nap)
        self.set_background_uv("tx_floor", self.floor_speed * nap)


class Bird(ImageWidget):
    ACCEL_FALL = 0.35
    ACCEL_JUMP = 7

    speed = NumericProperty(0)
    angle = AliasProperty(lambda self: 5 * self.speed, None, bind=["speed"])

    def gravity_on(self, height):
        self.pos_hint.pop("center_y", None)
        self.center_y = 0.6 * height

    def bump(self):
        self.speed = Bird.ACCEL_JUMP

    def update(self, nap):
        self.speed -= Bird.ACCEL_FALL
        self.y += self.speed


class ScoreWidget(Widget):
    pass


class Laser(ImageWidget):
    def move(self, nap):
        self.x += 360 * nap


class Missile(ImageWidget):
    LOWER_LIMIT = 384  # tx_floor height (96) + tx_grass (128) + bird height (80) * 2
    upper_limit = NumericProperty(0)

    def move(self, nap):
        self.x -= 180 * nap

    def get_y(self, height):
        self.upper_limit = height - 160  # bird height (80) * 2
        self.y = randint(self.LOWER_LIMIT, self.upper_limit)


class LevelCompleted(Popup):
    def on_open(self):
        anim = Animation(font_size=80, duration=.5)
        anim.start(self.ids.efficiency_label)
        
        anim2 = Animation(pos_hint={'center_x': .5}, duration=.5)
        anim2.start(self.ids.missiles_lounched_label)
        
        anim3 = Animation(pos_hint={'center_x': .5}, duration=.5)
        anim3.start(self.ids.missiles_destroyed_label)

    def get_label_color(self, variable):
        if variable < 50:
            self.ids.efficiency_label.color = get_color_from_hex("#FF0000")
        elif 50 <= variable < 75:
            self.ids.efficiency_label.color = get_color_from_hex("#FE6800")
        elif 75 <= variable < 90:
            self.ids.efficiency_label.color = get_color_from_hex("#FDD835")
        elif variable >= 90:
            self.ids.efficiency_label.color = get_color_from_hex("#00AD00")


class PtashkaApp(App):
    missiles_lounched = NumericProperty(0)
    missiles_destroyed = NumericProperty(0)
    missiles_missed = NumericProperty(0)
    player_efficiency = NumericProperty(0)
    missiles_lounched_label = StringProperty("")
    missiles_destroyed_label = StringProperty("")
    lasers = []
    missiles = []
    playing = BooleanProperty(False)
    level_completed = ObjectProperty(None)
    music_playing = BooleanProperty(False)
    sound_game_over = SoundLoader.load("sounds/game_over.wav")
    sound_flutter = SoundLoader.load("sounds/flutter.wav")
    sound_birds_singing = SoundLoader.load("sounds/birds_singing.wav")
    sound_alert_siren = SoundLoader.load("sounds/alert_siren.wav")
    sound_explosion = SoundLoader.load("sounds/explosion.wav")
    sound_level_completed = SoundLoader.load("sounds/level_completed.wav")
    music = SoundLoader.load("sounds/mixkit-daredevil-80.wav")

    def on_start(self):
        self.background = self.root.ids.background
        self.bird = self.root.ids.bird
        self.score_widget = self.root.ids.score_widget
        self.score_widget.opacity = 0
        self.music.loop = True
        self.music.volume = .6
        self.sound_explosion.volume = .8
        self.sound_alert_siren.volume = .5
        self.sound_birds_singing.loop = True
        self.sound_birds_singing.play()
        Clock.schedule_interval(self.update, 1 / 60)
        self.background.on_touch_down = self.user_action

    def update(self, nap):
        self.background.update(nap)
        if not self.playing:
            return
        self.background.floor_speed = 2
        self.background.grass_speed = 0.3
        self.bird.update(nap)
        self.check_hit()

        for laser in self.lasers:
            laser.move(nap)
            if laser.x > (self.root.width + laser.width):
                self.root.remove_widget(laser)
                self.lasers.remove(laser)

        for missile in self.missiles:
            missile.move(nap)
            if missile.x <= -missile.width:
                self.root.remove_widget(missile)
                self.missiles.remove(missile)
                self.missiles_missed += 1

        if (self.missiles_destroyed + self.missiles_missed) == 50:
            self.playing = False
            self.calc_player_efficiency()
            self.show_popup()
            self.game_over()

        if self.test_game_over():
            self.playing = False
            self.sound_game_over.play()
            self.game_over()

    def spawn_missiles(self, dt):
        if self.missiles_lounched < 50:
            missile = Missile(x=self.root.width)
            missile.get_y(self.root.height)
            self.root.add_widget(missile)
            self.missiles.append(missile)
            self.missiles_lounched += 1
        if not self.music_playing:
            self.music_playing = True
            self.sound_birds_singing.stop()
            self.music.play()

    def spawn_lasers(self):
        laser = Laser(
            x=(self.bird.x + self.bird.width / 2),
            y=(self.bird.y + self.bird.height / 2),
        )
        self.root.add_widget(laser)
        self.lasers.append(laser)

    def user_action(self, *args):
        if not self.playing:
            self.playing = True
            self.event = Clock.schedule_interval(self.spawn_missiles, 2)
            self.bird.source = "images/bird_of_war.png"
            self.bird.gravity_on(self.root.height)
            self.root.ids.title.color = (0, 0, 0, 0)
            self.score_widget.opacity = 1
            self.sound_alert_siren.play()
        self.bird.bump()
        self.sound_flutter.play()
        self.spawn_lasers()

    def check_hit(self):
        for missile in self.missiles:
            if (
                missile.x < (self.bird.x + self.bird.width)
                and missile.y < (self.bird.y + self.bird.height)
                and missile.y > self.bird.y
            ):
                self.root.remove_widget(missile)
                self.missiles.remove(missile)
                self.sound_explosion.play()
                self.missiles_destroyed += 1

        for missile in self.missiles:
            for laser in self.lasers:
                if missile.collide_point(laser.x + laser.width / 2, laser.y + laser.height / 2):
                    self.root.remove_widget(laser)
                    self.lasers.remove(laser)
                    self.root.remove_widget(missile)
                    self.missiles.remove(missile)
                    self.sound_explosion.play()
                    self.missiles_destroyed += 1

    def calc_player_efficiency(self):
        missiles_launched = self.missiles_destroyed + self.missiles_missed
        self.player_efficiency = int(self.missiles_destroyed / missiles_launched * 100)
        self.missiles_lounched_label = f"[b]{missiles_launched}[/b] missiles was launched"
        self.missiles_destroyed_label = f"[b]{self.missiles_destroyed}[/b] missiles was destroyed"

    def show_popup(self):
        self.level_completed = LevelCompleted()
        self.level_completed.get_label_color(self.player_efficiency)
        self.level_completed.open()
        self.sound_level_completed.play()

    def test_game_over(self):
        if self.bird.y < 96 or self.bird.y > self.root.height - self.bird.height:
            return True
        return False

    def game_over(self):
        self.event.cancel()
        for missile in self.missiles:
            self.root.remove_widget(missile)
        for laser in self.lasers:
            self.root.remove_widget(laser)
        self.bird.speed = 0
        self.bird.source = "images/bird.png"
        self.background.floor_speed = 0
        self.background.grass_speed = 0
        self.missiles_missed = 0
        self.missiles_destroyed = 0
        self.missiles_lounched = 0
        self.score_widget.opacity = 0
        self.root.ids.title.color = (0, 0, 0, 1)
        self.bird.pos_hint = {"center_x": 0.35, "center_y": 0.67}
        self.music_playing = False
        self.music.stop()
        self.sound_alert_siren.stop()
        self.sound_birds_singing.play()


if __name__ == "__main__":
    LabelBase.register(
        name="Rubik",
        fn_regular="fonts/Rubik-Regular.ttf",
        fn_bold="fonts/Rubik-Bold.ttf",
        fn_italic="fonts/Komika_display.ttf",
    )
    PtashkaApp().run()
